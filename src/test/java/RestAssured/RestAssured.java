package RestAssured;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
public class RestAssured {
  @Test
  public void postValue() {
	  
	  JSONObject req = new JSONObject();
      req.put("name","Ajay");
      req.put("email", "ajay@gmail.com");
      req.put("gender", "male");
      req.put("status", "Active");
      System.out.println(req);
      baseURI = "https://gorest.co.in/public/v2";
      given().log().all().contentType("application/json").header("authorization","Bearer ").body(req.toJSONString()).when().post("/users").then().statusCode(201);
  }
  }

